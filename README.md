# Simple PHP router
A simple single-file php router.

## Example

```PHP
require("../Route.php");

// if first character in string is "r" use regex
Route::get("r/(\d*)", function($params){
	echo "number is: " . $params[0];
});

// normaly use named paramaters
Route::get("/:type/:id", function($params){
	echo "type: " . $params["type"] . " id: " . $params["id"];
});

// normal paths
Route::get("/home", function(){
	echo "home";
});

// catch all request with an empty path for a 404 error
Route::all("", function(){
	header("HTTP/1.0 404 Not Found");
	echo "404: not found";
});
```

## HTTP Methods
Support for most HTTP methods

```PHP
Route::get("/home", function(){
	echo "home";
});

Route::post("/home", function(){
	echo "home";
});

Route::put("/home", function(){
	echo "home";
});

Route::delete("/home", function(){
	echo "home";
});
```
