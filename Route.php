<?php
class Route {
	public static function all($path, $callback){
		$uri = explode('?', $_SERVER['REQUEST_URI'], 2)[0];

		$useRegex = $path[0] === "r";
		if($useRegex){
			$path = substr($path, 1);
			$regex = $path;
		}else{
			// escape all symbols except : / # ?
			$regex = preg_replace("|([^\w\s:/#?])|", '\\\${1}', $path);
			// change names to regex
			$regex = preg_replace("|:\w+|", "([^/]+?)", $regex);

			preg_match_all("/:(\w+)/", $path, $paramNames);
			array_shift($paramNames);
		}

		if(empty($regex)){
			$regex = ".*";
		}

		$regex = "/^" . str_replace("/", "\/", $regex) . "$/";

		if (preg_match($regex, $uri, $params) === 1) {
			array_shift($params);

			if(!$useRegex){
				$params = array_combine($paramNames[0], $params);
			}

			call_user_func($callback, $params);
			die;
		}
	}

	public static function get($pattern, $callback){
		if($_SERVER["REQUEST_METHOD"] === "GET"){
			self::all($pattern, $callback);
		}
	}

	public static function post($pattern, $callback){
		if($_SERVER["REQUEST_METHOD"] === "POST"){
			self::all($pattern, $callback);
		}
	}

	public static function delete($pattern, $callback){
		if($_SERVER["REQUEST_METHOD"] === "DELETE"){
			self::all($pattern, $callback);
		}
	}

	public static function put($pattern, $callback){
		if($_SERVER["REQUEST_METHOD"] === "PUT"){
			self::all($pattern, $callback);
		}
	}

}
